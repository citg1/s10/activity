package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@SpringBootApplication
@RestController
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}

	// Get all users
//	localhost:8080/users
//	@RequestMapping(value="/users", method = RequestMethod.GET)
	@GetMapping("/users")
	public String getUsers(){
		return "All users retrieved";
	}

	// Create User
//	localhost:8080/users
//	@RequestMapping(value = "/users", method = RequestMethod.POST)
	@PostMapping("/users")
	public String createUser(){
		return "New user created.";
	}

	// Get a specific user
//	localhost:8080/myUsers
//	@RequestMapping(value = "/myUsers", method = RequestMethod.GET)
	@GetMapping("/myUser/{userid}")
	public String getPost(@PathVariable Long userid){
		return "Viewing details of the user " + userid;
	}





	// Delete a user
//	localhost:8080/users/1234
//	@RequestMapping(value = "users/{userid}", method = RequestMethod.DELETE)
	@DeleteMapping("/users/{userid}")
	public String deletePost(@PathVariable Long userid){
		return "The user " + userid + " has been deleted.";
	}


	// Update user
//	localhost:8080/users/1234
//	@RequestMapping(value = "/posts/{userid}", method = RequestMethod.PUT)
//	Automatically converts the format to JSON.
	@PutMapping("/users/{userid}")
	@ResponseBody
	public Users updateUser(@PathVariable Long userid, @RequestBody Users user){
		return user;
	}
}
